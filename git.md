<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
git 分布式版本管理工具
svn 集中式版本管理工具

1. 下载安装

```shell
git --version
```
检测是否安装

----

2. git 工作流程

- 本地工作区： 本地文件夹（当前文件夹根目录需要有.git文件夹）
- 暂存区： 把文件状态暂存到.git文件夹里面
- 历史版本区: 产生commitid 可以通过commitid回到任意文件状态

- 第三方托管平台
1. github
2. gitlab(公司内用gitlab比较多)
3. gitee 码云

----

3. 基础配置

- 查看所有配置信息

```shell
git config --list
```

- 必须要配置user.name user.email
```shell
git config --global user.name yh
git config --global user.email yh
```

----

4. 实际操作

- 查看工作区状态

```shell
git status
```  

5. fork使用

git 图形界面工具

fork

sourcetree

简化指令操作